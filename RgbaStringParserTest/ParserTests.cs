using NUnit.Framework;
using RgbaStringParser;

namespace RgbaStringParserTest;

[TestFixture]
public class ParserTests
{
    [Test]
    public void BasicRgbaStringsTest()
    {
        var parser = new RgbaParser();
        var basicColor = parser.Parse("rgba(100,200,255,0.5)");
        
        Assert.AreEqual(100, basicColor.Red);
        Assert.AreEqual(200,basicColor.Green);
        Assert.AreEqual(255, basicColor.Blue);
    }

    [Test]
    public void ParsesRgbaStringWithWhitespace()
    {
        var parser = new RgbaParser();
        var basicColor = parser.Parse("rgba(100,  200, 255, 0.5)");
        
        Assert.AreEqual(100, basicColor.Red);
        Assert.AreEqual(200,basicColor.Green);
        Assert.AreEqual(255, basicColor.Blue);
    }
}