﻿namespace RgbaStringParser;

public static class Program
{
    public static void Main()
    {
        var x = new RgbaParser().Parse("rgba(100,10,11,0.1)");
        Print(x);
    }

    private static void Print(RgbaColor c)
    {
        Console.WriteLine($"{c.Red} {c.Green} {c.Blue} {c.Alpha}");
    }
}