namespace RgbaStringParser;

using Pidgin;
using static Pidgin.Parser;
using static Pidgin.Parser<char>;

public class RgbaParser
{
    public RgbaColor Parse(string input)
    {
        var rgbaStart = String("rgba(");

        var rgbaEnd = String(")");
        var comma = String(",");

        var colorWithComma = DecimalNum.Before(comma).Before(Whitespaces.Optional());
        var main = Map((r, g, b, a) => new RgbaColor() {Red = r, Blue = b, Green = g, Alpha = a}, 
            colorWithComma, colorWithComma, colorWithComma, Real);
        var parser = rgbaStart.Then(main).Before(rgbaEnd);
        
        var color = parser.ParseOrThrow(input);
        return color;
    }
}